====================================
 README for APK Fission Package Set
====================================
:Authors:
  * **Síle Ekaterin Liszka**, Project Lead
  * **APK Fission Contributors**, contributions
:Status:
  Production
:Copyright:
  © 2018-2024 APK Fission. NCSA open source license.

Introduction
============

This repository contains the APK Fission package set. It is used by APK
Fission build system to create repositories to be used by Adélie's APK 
package manager.

Licences
````````
As APK Fission is a repository for open-source Linux software, packages
contained in in this repository must also be provided under a licence
recognised by the OSI_. No exceptions will be made.

.. _OSI: http://opensource.org/licenses/category

Changes
```````
Any changes to this repository - additions, removal, or version bumps - 
must be reviewed before being pushed to the ``main`` branch.  There are
no exceptions to this rule.

Contents
========

This section contains a high-level view of the contents of this
repository. It does not list every package available; it is merely a
guide to help you find what you need.

``main``: Open-Source Packages
``````````````````````````````

The ``main`` directory contains software which is not packaged by
Adélie. Principally, this is because the software does not build on all
Tier 1 architectures as Adélie requires, but it may also be due to
licence restrictions specified by the software developers which do not
meet Adélie's strict requirements to be considered for inclusion.

``nonfree``: Non-Free Packages
``````````````````````````````

The ``nonfree`` directory contains software which is not distributed
under a Free or Open-Source licence but which is nevertheless provided
under terms that permit free redistribution. This can include firmware
for hardware that is otherwise supported by Adélie's ``easy-kernel``
package.

``legacy``: Legacy/Abandoned Packages
`````````````````````````````````````

The ``legacy`` directory contains software which is no longer packaged
by APK Fission, for various reasons. These packages are retained for
archival reasons.

Usage
=====

This section contains usage information for this repository.

As an overlay
`````````````

APK Fission is presently designed specifically to be used with the
Adélie Linux distribution only. Certain packages may be used with other
APK-based distributions, such as Alpine Linux, but no support will be
provided.

The domain ``distfiles.apkfission.net`` is a round-robin for all
available APK Fission mirrors. You may add a repository named above to
``/etc/apk/repositories``:

::

  https://distfiles.apkfission.net/adelie/$version/$repo

Where ``$version`` is the version of Adélie Linux you are using, or
``current`` for a rolling-release style distribution (which may be
unstable - you have been warned!).

``$repo`` should be replaced with the name of the repository you are
wanting to use. ``main`` is required to use APK Fission packages, as it
contains the ``apkfission-keys`` package, which contains current public
keys for APK Fission repositories.

Run ``apk update`` to update the package index on your local system.
The packages will then be available to you.

N.B. If you installed Adélie using their graphical installer and chose
to include firmware support, you are already using APK Fission and do
not need to do anything to use packages provided by APK Fission.
