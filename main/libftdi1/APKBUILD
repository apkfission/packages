# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=libftdi1
pkgver=1.5
pkgrel=0
pkgdesc="Library for working with FTDI chips"
url="https://www.intra2net.com/en/developer/libftdi/"
arch="all"
license="LGPL-2.1+ AND LGPL-2.1-only AND MIT AND GPL-2.0-only"
depends=""
makedepends="boost-dev cmake confuse-dev doxygen graphviz libusb-dev
	python3-dev swig"
subpackages="$pkgname-dev $pkgname-doc"
source="https://www.intra2net.com/en/developer/libftdi/download/libftdi1-$pkgver.tar.bz2"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DLIB_SUFFIX="" \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Wno-dev \
		-DBUILD_TESTS=ON \
		-DDOCUMENTATION=ON \
		-DEXAMPLES=OFF \
		-DFTDIPP=ON \
		-DPYTHON_BINDINGS=ON \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c525b2ab6aff9ef9254971ae7d57f3549a36a36875765c48f947d52532814a2a004de1232389d4fe824a8c8ab84277b08427308573476e1da9b7db83db802f6f  libftdi1-1.5.tar.bz2"
